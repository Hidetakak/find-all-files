import os
import datetime
import sys
from optparse import OptionParser

MAX_FOLDER_DEPTH = 50           # フォルダ階層の最大深さ（現実的にはここまであれば十分と判断）

def findAllFiles(root_path):
    for dir_name, sub_dirs, filenames in os.walk(root_path):
        folder_depth = 0
        for dir_name_char in dir_name:
            if dir_name_char == os.sep:
                folder_depth += 1
        yield (True, folder_depth, dir_name, os.stat(dir_name), len(sub_dirs))
        for file in filenames:
            filename = os.path.join(dir_name, file)
            yield (False, folder_depth + 1, filename, os.stat(filename), 0)

def makeTreeStr(isFolder, filename, depth, not_yet_printed_subdirs, num_sub_dirs):

    if isFolder:                # フォルダの場合はフォルダツリーの情報を更新する
        not_yet_printed_subdirs[depth] -= 1 # 同じ階層のフォルダの残り数
        not_yet_printed_subdirs[depth + 1] = num_sub_dirs # 一つ下の階層のフォルダの残り数

    tree_and_filename = ""      # ツリーとフォルダ・フォルダ名を格納する文字列
    
    if depth == 0:          # ルートフォルダの場合の処理（ツリー表示なしで名称のみ表示)
        tree_and_filename += filename
        return tree_and_filename

    for folder_depth in range(1, depth): # ツリーの表示
        if not not_yet_printed_subdirs[folder_depth] == 0:
            tree_and_filename += "　┃" # まだ表示していないサブディレクトリが残っている階層
        else:
            tree_and_filename += "　　" # まだ表示していないサブディレクトリがない階層
    
    if isFolder:
        if not_yet_printed_subdirs[depth] == 0:
            tree_and_filename += ("　┗ " + filename) # このフォルダが末っ子の場合
        else:
            tree_and_filename += ("　┣ " + filename) # まだ同じ階層にフォルダがある場合
        return tree_and_filename
    else:                       # ファイルの場合は、ツリーの枝には結ばずにインデントして表示する
        if not not_yet_printed_subdirs[depth] == 0:
            tree_and_filename += ("　┃　　" + filename)
        else:
            tree_and_filename += ("　　　　" + filename)
        return tree_and_filename


# コマンドライン引数から対象フォルダを決定する
parser = OptionParser()
parser.add_option("--target", action = "store", type = "string", dest = "target") # フォルダ一覧を作る対象
parser.add_option("--dest", action = "store", type = "string", dest = "dest") # フォルダ一覧を保存するフォルダ
parser.add_option("--out", action = "store", type = "string", dest = "output_file_name") # 保存するフォルダ一覧のファイル名(拡張子なし)
(option, args) = parser.parse_args()

if option.target is None:
    root_path = os.getcwd()     # ターゲットの指定が無ければ、カレントフォルダを対象とする
else:
    root_path = option.target
if option.dest is None:
    save_path = root_path       # 出力先の指定がなければ、対象フォルダに一覧表を出力する
else:
    save_path = option.dest
if option.output_file_name is None:
    option.output_file_name = "filelist"

if not os.path.exists(root_path): # 対象フォルダが存在するか検査
    sys.exit("対象フォルダが存在しません")
if not os.path.exists(save_path): # 出力先フォルダが存在するか検査
    sys.exit("出力先フォルダが存在しません")

# 一覧表のファイル名を日付から生成し、ファイルオープン
today = datetime.datetime.today()
output_file_name = today.strftime("%Y%m%d_") + option.output_file_name + ".csv"
output_file = open(save_path + os.sep + output_file_name, "w")

# 一覧表生成
print("最終更新日時,タイプ,ツリー構造,ファイルサイズ,拡張子,フルパス", file = output_file)
root_depth = 0
not_yet_printed_subdirs = [0 for x in range(MAX_FOLDER_DEPTH)]

for root_path_char in root_path: # ルートフォルダの階層の深さ
    if root_path_char == os.sep:
        root_depth += 1
for isFolder, depth, full_path, stat, num_sub_dirs in findAllFiles(root_path):
    depth -= root_depth         # ルートフォルダからの相対的な深さ
    if depth < 2:
        print("processing ", full_path) # 進捗表示
    filename = os.path.split(full_path)[1]
    last_modified = stat.st_mtime
    dt = datetime.datetime.fromtimestamp(last_modified)
    print(dt.strftime("%Y/%m/%d %H:%M:%S,"), end = "", file = output_file)
    if isFolder:
        print("<FOLDER>", end = "", file = output_file)
    else:
        print("<FILE>", end = "", file = output_file)
    print(",", makeTreeStr(isFolder, filename, depth, not_yet_printed_subdirs, num_sub_dirs), end = "", file = output_file)
    if isFolder:
        print(",,", file = output_file, end = ""); # フォルダに対しては、ファイルサイズ、拡張子は空白にする
    else:
        print(",", stat.st_size, ",", os.path.splitext(filename)[1], file = output_file, end = "")
    print(",", full_path, file = output_file)
output_file.close()
